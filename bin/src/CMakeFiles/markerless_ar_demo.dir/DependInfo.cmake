# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/src/ARDrawingContext.cpp" "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/bin/src/CMakeFiles/markerless_ar_demo.dir/ARDrawingContext.cpp.o"
  "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/src/ARPipeline.cpp" "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/bin/src/CMakeFiles/markerless_ar_demo.dir/ARPipeline.cpp.o"
  "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/src/CameraCalibration.cpp" "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/bin/src/CMakeFiles/markerless_ar_demo.dir/CameraCalibration.cpp.o"
  "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/src/GeometryTypes.cpp" "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/bin/src/CMakeFiles/markerless_ar_demo.dir/GeometryTypes.cpp.o"
  "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/src/Pattern.cpp" "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/bin/src/CMakeFiles/markerless_ar_demo.dir/Pattern.cpp.o"
  "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/src/PatternDetector.cpp" "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/bin/src/CMakeFiles/markerless_ar_demo.dir/PatternDetector.cpp.o"
  "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/src/main.cpp" "/Users/haoranzhi/Desktop/Chapter3_MarkerlessAR/bin/src/CMakeFiles/markerless_ar_demo.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv"
  "/usr/local/include"
  "../src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
